FROM debian:latest

MAINTAINER Thomas Zayouna<tzayouna@gmail.com>

EXPOSE 22

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update
RUN apt-get install -y \
	openssh-server \
	git

RUN ssh-keygen -t rsa -b 4096 -C "hello@docker.com" -N '' -f ~/.ssh/id_rsa
RUN adduser git
RUN  mkdir -p /home/git/.ssh \
	 && chmod 700 /home/git/.ssh \
	 && touch /home/git/.ssh/authorized_keys \
	 && chmod 600 /home/git/.ssh/authorized_keys \
	 && mkdir /home/git/workshop.git \
	 && cd /home/git/workshop.git \
	 && git init --bare \
	 && echo $(pwd) 

RUN cat ~/.ssh/id_rsa.pub >> /home/git/.ssh/authorized_keys
RUN chown -R git: /home/git

COPY entrypoint.sh /entrypoint.sh

CMD ["sh", "/entrypoint.sh"]
